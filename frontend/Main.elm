module Main exposing (..)

import Html
import Message exposing (Msg)
import State exposing (State)
import View exposing (view)
import Update exposing (update)
import Util exposing ((=>))


init : ( State, Cmd Msg )
init =
    0 => Cmd.none


main : Program Never State Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }
