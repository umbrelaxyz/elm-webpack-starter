module Update exposing (..)

import Message exposing (Msg(..))
import State exposing (State)


update : Msg -> State -> ( State, Cmd Msg )
update msg state =
    case msg of
        NoOp ->
            state ! []

        Increment ->
            (state + 1) ! []
