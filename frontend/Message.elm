module Message exposing (Msg(..))


type Msg
    = NoOp
    | Increment
