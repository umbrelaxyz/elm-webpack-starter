module View exposing (view)

import String
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Message exposing (Msg(..))
import State exposing (State)
import Util exposing ((=>))


view : State -> Html Msg
view state =
    layout (heroView state)



-- INTERNAL --


layout : Html Msg -> Html Msg
layout content =
    div [ class "container has-text-centered" ]
        [ header
        , content
        , footer
        ]


header : Html Msg
header =
    div [] []


footer : Html Msg
footer =
    div [] []


heroView : State -> Html Msg
heroView state =
    div [ class "hero" ]
        [ div [ class "hero-body" ]
            [ img [ src "assets/img/elm.jpg", style styles.heroImg ] []
            , helloView state
            , div [ class "subtitle" ] [ text ("Elm Webpack Starter") ]
            , button [ class "button is-primary", onClick Increment ]
                [ faIcon "user"
                , span [] [ text "FTW!" ]
                ]
            ]
        ]


helloView : State -> Html Msg
helloView state =
    div
        [ class "title" ]
        [ text ("Hello, Elm" ++ ("!" |> String.repeat state)) ]


faIcon : String -> Html msg
faIcon iconName =
    span [ class "icon" ]
        [ i [ class ("fa fa-" ++ iconName) ] []
        ]



-- STYLES --


styles : { heroImg : List ( String, String ) }
styles =
    { heroImg =
        [ "width" => "33%"
        , "border" => "4px solid #337AB7"
        ]
    }
